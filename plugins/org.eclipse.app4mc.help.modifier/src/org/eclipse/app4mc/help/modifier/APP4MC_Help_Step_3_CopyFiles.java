/**
 ********************************************************************************
 * Copyright (c) 2018-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * Create the final folder structure and copies the files to the correct
 * locations
 * <p>
 * STEPS:
 * <ul>
 * <li>move
 * <ul>
 * <li>pdf files to pdf folder
 * <li>move zip files to zip folder
 * <li>move javadoc files to javadoc folder
 * <li>move help.css to css folder
 * </ul>
 * <li>update links
 * <li>apply template
 * </ul>
 */
public class APP4MC_Help_Step_3_CopyFiles {

	public static void execute(final String imagesFolderPath, final String htmlFolderPath, final String outputFolderPath, final String version) {

		final Path imagesFolder = Paths.get(imagesFolderPath);
		final Path htmlFolder = Paths.get(htmlFolderPath);
		if (!Files.exists(imagesFolder) || !Files.exists(htmlFolder)) {
			System.err.println("Provide valid input folders");
			System.exit(-1);
		}

		final Path outputFolder = Paths.get(outputFolderPath);
		try {
			Files.createDirectories(outputFolder);
		} catch (IOException e) {
			e.printStackTrace();
			System.err.println("Could not create: " + outputFolderPath);
		}

		copyFiles(imagesFolder, outputFolderPath);

		updateLinks(htmlFolderPath);

		applyTemplate(htmlFolderPath, outputFolderPath, version);
	}


	private static void copyFiles(final Path imagesFolder, final String outputFolderPath) {
			try {
				// css
	
				Path cssInput = Paths.get(new File("static/css").getAbsolutePath());
				Path cssOutput = Files.createDirectories(Paths.get(outputFolderPath, "css"));
	
				List<Path> sourceFiles;
				try (Stream<Path> filesStream = Files.walk(cssInput)) {
					sourceFiles = filesStream.toList();
				}
	
				for (Path input : sourceFiles) {
					copyFileToDirectory(input, cssOutput);					
				}
	
				// javadoc
	
	//			Path javadocOutput = Files.createDirectories(Paths.get(outputFolderPath, "javadoc"));
	
				// images, zip, pdf
	
				Path imagesOutput = Files.createDirectories(Paths.get(outputFolderPath, "images"));
				Path pdfOutput = Files.createDirectories(Paths.get(outputFolderPath, "pdf"));
				Path zipOutput = Files.createDirectories(Paths.get(outputFolderPath, "zip"));
	
				Files.walkFileTree(imagesFolder, new SimpleFileVisitor<Path>() {
					@Override
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						if (isImage(file)) {
							copyFileToDirectory(file, imagesOutput);
						} else if (file.toString().endsWith(".pdf")) {
							copyFileToDirectory(file, pdfOutput);
						} else if (file.toString().endsWith(".zip")) {
							copyFileToDirectory(file, zipOutput);
						} else if (file.toString().endsWith("help.css")) {
							copyFileToDirectory(file, cssOutput);
						}
						return FileVisitResult.CONTINUE;
					}
	
					private boolean isImage(Path file) {
						String filename = file.toFile().getName();
						return filename.endsWith(".png") || filename.endsWith(".svg") || filename.endsWith(".gif");
					}
				});
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}


	private static void copyFileToDirectory(Path file, Path directory) throws IOException {
		if (Files.isRegularFile(file) && Files.isDirectory(directory)) {
			Files.copy(file, Paths.get(directory.toString(), file.toFile().getName()), StandardCopyOption.REPLACE_EXISTING);			
		}
	}


	/**
	 * Update the links of specific type of elements that are stored in separate folders: .zip, .pdf
	 * 
	 * input:	app4mc-help_2_updated_content.html
	 * output:	app4mc-help_3_updated_links.html
	 * 
	 * @param htmlFolder
	 */
	private static void updateLinks(String htmlFolderPath) {

		String types = "zip|pdf";

		try {
			// read
			String content = Files.readString(Paths.get(htmlFolderPath, "app4mc-help_2_updated_content.html"));

			// modify
			StringBuilder sb = new StringBuilder();
			final Pattern pattern = Pattern.compile("href=\"images/(?<name>.*?)\\.(?<ext>" + types + ")\"");
			final Matcher matcher = pattern.matcher(content);
			while (matcher.find()) {
				matcher.appendReplacement(sb,"href=\"${ext}/${name}.${ext}\"");
			}
			matcher.appendTail(sb);

			// write
			Files.write(Paths.get(htmlFolderPath, "app4mc-help_3_updated_links.html"), sb.toString().getBytes(StandardCharsets.UTF_8));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Loads template, replaces variable parts, stores result.
	 * 
	 * input:	app4mc-help_toc.html
	 * 			app4mc-help_3_updated_links.html
	 * output:	help.html
	 * 
	 * @param htmlFolder
	 * @param outputFolderPath
	 */
	private static void applyTemplate(String htmlFolderPath, String outputFolderPath, String version) {

		Path template = Paths.get(new File("template/help.html").getAbsolutePath());
		Path toc = Paths.get(htmlFolderPath, "app4mc-help_toc.html");
		Path content = Paths.get(htmlFolderPath, "app4mc-help_3_updated_links.html");
		Path output = Paths.get(outputFolderPath, "help.html");

		try {
			// read
			String result = Files.readString(template);
			String tocTree = Files.readString(toc);
			String articles = Files.readString(content);

			// strip content
			tocTree = tocTree.substring(tocTree.indexOf("<ul>"), tocTree.lastIndexOf("</ul>") + 5);
			articles = articles.substring(articles.indexOf("<article>"), articles.lastIndexOf("</article>") + 10);

			// replace placeholders in template
			result = result.replace("{$current-year}", String.valueOf(LocalDate.now().getYear()));
			result = result.replace("{$app4mc-version}", version);
			result = result.replace("{$app4mc-help-toc}", tocTree);
			result = result.replace("{$app4mc-help-content}", articles);

			// write
			Files.write(output, result.getBytes(StandardCharsets.UTF_8));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

/**
 ********************************************************************************
 * Copyright (c) 2018-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier;

import java.io.File;

public class Run_automated_steps {

	static final String BASE_PATH = "C:/workspace/httrack/app4mc-help-3.2.0";
	static final String PROJ_VERSION = "3.2";

	static final String PROJ_NAME = "APP4MC " + PROJ_VERSION + " Help";
	static final String FOLDER_NAME = "app4mc-" + PROJ_VERSION + "-help";
	
	public static void main(final String[] args) throws Exception {

		String inputFolder = BASE_PATH + "/help-raw/" + PROJ_NAME + "/web";
		String inputFile = inputFolder + "/printc223.html";

		String imagesFolder = inputFolder + "/images";

		String intermediateFolder = BASE_PATH + "/help-intermediate";

		String finalFolder = BASE_PATH + "/help-final/" + FOLDER_NAME;

		// STEP 1
		final File fileStep1 = APP4MC_Help_Step_1_UpdateTOC.execute(inputFile, intermediateFolder);

		// STEP 2
		APP4MC_Help_Step_2_UpdateContent.execute(fileStep1.getAbsolutePath());

		// STEP 3
		APP4MC_Help_Step_3_CopyFiles.execute(imagesFolder, intermediateFolder, finalFolder, PROJ_VERSION);
	}

}

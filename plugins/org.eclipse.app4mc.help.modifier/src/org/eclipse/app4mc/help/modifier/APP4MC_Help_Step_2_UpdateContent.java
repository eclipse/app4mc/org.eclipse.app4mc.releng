/**
 ********************************************************************************
 * Copyright (c) 2018-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class APP4MC_Help_Step_2_UpdateContent {

	public static void main(final String[] args) throws IOException {

		String inputFilePath = "";

		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("Provide the location of input HTML file :");
			inputFilePath = scanner.nextLine(); // "C:/workspace/work/app4mc-help/app4mc-help-0.9.6/help-raw/APP4MC Help 0.9.6/web/app4mc-help_1_updated_headings.html";
		}

		execute(inputFilePath);

	}


	public static File execute(final String inputFilePath) throws IOException {
		final File inputFile = new File(inputFilePath);

		if (!inputFile.exists()) {
			System.err.println("provide a valid input HTML location");
			System.exit(-1);
		}

		final File inputFolder = inputFile.getParentFile();

		final StringBuilder sb = new StringBuilder();


		/*-
		 * ======================================================================
		 * 					STEP 1 : merging sub HTML files into a single HTML
		 * ======================================================================
		 */

		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(inputFile), StandardCharsets.UTF_8))) {


			boolean isInBody = false;

			boolean isBodyTagFound = false;

			System.out.println("Reading input file : " + inputFilePath);

			while (br.ready()) {

				final String readLine = br.readLine();

				if (readLine.trim().startsWith("<body")) {

					sb.append(readLine);
					sb.append(System.getProperty("line.separator"));
					isInBody = true;
					isBodyTagFound = true;
				}
				else if (readLine.contains("</body>")) {
					isInBody = false;
					sb.append(readLine);
					sb.append(System.getProperty("line.separator"));
				}
				else if (isInBody) {
					sb.append(readLine);
					sb.append(System.getProperty("line.separator"));
				}

				if (isBodyTagFound == false) {
					// Copying the entire content from start till "body" tag is reached
					sb.append(readLine);
					sb.append(System.getProperty("line.separator"));
				}
			}

			sb.append("</html>");

		}
		catch (final Exception e) {

			e.printStackTrace();

			System.err.println("Error reading HTML file : " + inputFilePath);
		}


		/*-
		 * ======================================================================
		 * 					STEP 2 : modifying the contents of HTML String
		 * ======================================================================
		 */

		String htmlString = fixHTMLContent(sb);


		/*-
		 * ======================================================================
		 * 					STEP 3 : generating output HTML file
		 * ======================================================================
		 */

		final File outputFile = new File(inputFolder, "app4mc-help_2_updated_content.html");

		try (BufferedWriter bw = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(outputFile), StandardCharsets.UTF_8))) {
			bw.write(htmlString);

			System.out.println("Generated modified HTML file at : " + outputFile.getAbsolutePath());

		}
		catch (final Exception e) {
			e.printStackTrace();

			System.err.println("Error writing file : " + outputFile.getAbsolutePath());
		}

		return outputFile;
	}


	private static String fixHTMLContent(final StringBuilder sb) {

		String htmlString = sb.toString();

		final Map<String, String> map = prepareReplacementContent(sb);

		final Iterator<Entry<String, String>> iterator = map.entrySet().iterator();

		while (iterator.hasNext()) {
			final Entry<String, String> next = iterator.next();

			htmlString = htmlString.replace(next.getKey(), next.getValue());
		}

		return htmlString;
	}


	private static Map<String, String> prepareReplacementContent(final StringBuilder sb) {

		final Map<String, String> original_replacement_map = new LinkedHashMap<>();

		/*-
		 * ==================================================================
		 * 						STEP 1 : removing print dialog action
		 * ==================================================================
		 */
		original_replacement_map.put("onload=\"print()\"", "");


		/*-
		 * ==================================================================
		 * 						STEP 2 : fixing old xhtml tags
		 * ==================================================================
		 */

		original_replacement_map.put("<img border=\"0\"", "<img");
		original_replacement_map.put("<br clear=\"none\"/>", "<br>");
		original_replacement_map.put("<br clear=\"none\" />", "<br>");
		original_replacement_map.put(" shape=\"rect\">", ">");

		/*-
		 * ==================================================================
		 * 						STEP 3 : fixing special characters
		 * ==================================================================
		 */

		original_replacement_map.put("“", "\"");
		original_replacement_map.put("”", "\"");
		original_replacement_map.put("‘", "'");
		original_replacement_map.put("’", "'");
		original_replacement_map.put("→", "&rarr;");

		/*-
		 * ==================================================================
		 * 						STEP 4 : fixing body / article structure
		 * ==================================================================
		 */

		original_replacement_map.put("\t<body>", "<article>");
		original_replacement_map.put("\t</body>", "</article>");
		
		// move blank lines
		original_replacement_map.put("<article>\r\n\r\n", "\r\n\r\n<article>");
		
		// only keep <article> in front of <h1
		original_replacement_map.put("</article>\r\n\r\n\r\n<article>\r\n<h2", "\r\n\r\n<h2");
		original_replacement_map.put("</article>\r\n\r\n\r\n<article>\r\n<h3", "\r\n\r\n<h3");
		original_replacement_map.put("</article>\r\n\r\n\r\n<article>\r\n<h4", "\r\n\r\n<h4");
		original_replacement_map.put("</article>\r\n\r\n\r\n<article>\r\n<h5", "\r\n\r\n<h5");
		original_replacement_map.put("</article>\r\n\r\n\r\n<article>\r\n<h6", "\r\n\r\n<h6");
		
		/*-
		 * =========================================================================================================================================
		 * 				STEP 4 : Replace links with server location as prefix  with http
		 *
		 * (e.g: http://127.0.0.1:58400/help/topic/org.eclipse.app4mc.amalthea.converters.help/help/http should be replaced with http )
		 * =========================================================================================================================================
		 */

		/*-
		 * modifying the references to other HTML pages
		 * Example:
		 * <a href="http://127.0.0.1:58400/help/topic/org.eclipse.app4mc.amalthea.converters.help/help/http://www.amalthea-project.org/index.php/roadmap" shape="rect">www.amalthea-project.org</a> )
		 *
		 * Should be changed to
		 *
		 *  <a href="http://www.amalthea-project.org/index.php/roadmap" shape="rect">www.amalthea-project.org</a> )
		 */

		final Pattern patternforHyperLinks = Pattern
				.compile("http\\://127\\.0\\.0\\.1\\:(\\d?)*\\/help\\/topic\\/(.*)\\/http");
		final Matcher matcherForHyperLinks = patternforHyperLinks.matcher(sb.toString());

		while (matcherForHyperLinks.find()) {
			final String group = matcherForHyperLinks.group();
			// System.out.println(group);
			original_replacement_map.put(group, "http");
		}

		/*-
		 * ======================================================================================================================================================================
		 * 				STEP 5 : Replace links which points to other APP4MC html files with anchor
		 *
		 * (e.g: http://127.0.0.1:58883/help/topic/org.eclipse.app4mc.amalthea.model.help/help/user_sw_runtime.html#user-sw-runtime should be replaced with #user-sw-runtime )
		 * (e.g: http://127.0.0.1:58883/help/topic/org.eclipse.app4mc.amalthea.workflow.help/help/#GenerateMapping should be replaced with #GenerateMapping )
		 * ======================================================================================================================================================================
		 */

		/*-
		 * modifying the references to other HTML pages (inside app4mc)
		 *
		 * Example:
		 * http://127.0.0.1:58883/help/topic/org.eclipse.app4mc.amalthea.model.help/help/user_sw_runtime.html#user-sw-runtime
		 *
		 * Should be changed to local anchor
		 *
		 * #user-sw-runtime
		 */
		final Pattern patternforHyperLinks2 = Pattern.compile(
				"(http\\://127\\.0\\.0\\.1\\:(\\d?)*\\/help\\/topic\\/(.*?)\\.html\\#)|(http\\://127\\.0\\.0\\.1\\:(\\d?)*\\/help\\/topic\\/(.*?)\\#)");
		final Matcher matcherForHyperLinks2 = patternforHyperLinks2.matcher(sb.toString());

		while (matcherForHyperLinks2.find()) {
			final String group = matcherForHyperLinks2.group();
			System.out.println(group);
			original_replacement_map.put(group, "#");
		}

		/*-
		 * =========================================================================================================================================
		 * 				STEP 6 : Replace links with server location with ".." string
		 *
		 * (e.g: http://127.0.0.1:58400/help/topic/org.eclipse.app4mc.amalthea.converters.help/help should be replaced with ".." )
		 * =========================================================================================================================================
		 */

		/*-
		 * modifying the references to other HTML pages (inside app4mc)
		 *
		 * Example:
		 * http://127.0.0.1:58400/help/topic/org.eclipse.app4mc.amalthea.converters.help/util_plugins/1.0.3_org.itea2.amalthea.urifragments.converter.zip
		 *
		 * Should be changed to
		 *
		 * ../topic/org.eclipse.app4mc.amalthea.converters.help/util_plugins/1.0.3_org.itea2.amalthea.urifragments.converter.zip
		 */
		final Pattern patternforHyperLinks3 = Pattern.compile("http\\://127\\.0\\.0\\.1\\:(\\d?)*\\/help");
		final Matcher matcherForHyperLinks3 = patternforHyperLinks3.matcher(sb.toString());

		while (matcherForHyperLinks3.find()) {
			final String group = matcherForHyperLinks3.group();
			// System.out.println(group);
			original_replacement_map.put(group, "..");
		}

		return original_replacement_map;
	}


}

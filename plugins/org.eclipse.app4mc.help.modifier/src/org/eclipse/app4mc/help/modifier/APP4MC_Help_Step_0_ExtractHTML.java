/**
 ********************************************************************************
 * Copyright (c) 2018-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier;

public class APP4MC_Help_Step_0_ExtractHTML {

	
/******************************************************************
 * 
 * Tool: HTTrack (https://www.httrack.com/)
 * 
 * Free software offline browser
 * 
 ******************************************************************


 ******************************************************************
 *  Command to start httrack (from bash)
 ******************************************************************

mkdir app4mc-help-3.2.0
cd app4mc-help-3.2.0

httrack "http://127.0.0.1:53792/help/advanced/print.jsp?topic=/../nav/3" \
-O "./help-raw/APP4MC 3.2 Help" -N1 -v \
+*.png  +*.svg +*.gif +*.jpg +*.jpeg +*.zip + *.pdf +*.css +*.js

 ******************************************************************


 ******************************************************************
 *  WinHTTrack: Add the following lines in tab "Scan Rules"
 ******************************************************************

+*.zip
+*.svg
+*.pdf

 ******************************************************************
 */


}

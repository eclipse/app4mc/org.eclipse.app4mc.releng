/**
 ********************************************************************************
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.help.modifier.util;

import java.util.ArrayList;
import java.util.List;

public class HeadingIterator {
	private List<Heading> headings = new ArrayList<Heading>();
	private int index = 0;

	public HeadingIterator(List<Heading> headings) {
		super();
		this.headings = headings;
	}

	public Heading current() {
		return get(index);
	}

	public Heading next() {
		index++;
		return get(index);
	}

	public Heading preview() {
		return get(index + 1);
	}

	private Heading get(int i) {
		if (i >= headings.size())
			return new Heading(0, "", "");

		return headings.get(i);
	}
}


# Eclipse APP4MC - Releng helpers

This is the repository of helper projects related to Eclipse APP4MC:

    org.eclipse.app4mc.help.modifier      | Builds an offline HTML help based on the Eclipse Help
    org.eclipse.app4mc.release.helper     | Generates a list of validations (based on annotations)


## License

[Eclipse Public License (EPL) v2.0][1]

[1]: https://www.eclipse.org/legal/epl-2.0/